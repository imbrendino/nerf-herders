# NERF HERDERS coding challange

## Project Structure
The application is composed in 3 parts

 - nerf-herders-migration
 - nerf-herders-backend
 - nerf-herders-frontend

# Startup
First of all you need to run you Neo4J instance that is needed to run the application

# nerf-herders-migration

The migration module is used to load the data in the database from a file that is located in the same folder

> data.json

Before running the migration you need to change the config file with the access data for your database. The file is

> db-config.json

After set up everything what you need to do is to run

> npm install

 to install all the dependencies and now we are ready to load the data with :
> migrate up

if you want to rollback you can run

> migrate down


# nerf-herders-backend

This module is the backend part of the application.

To install the dependecies please run the following command from the folder

> npm install

Before running the backend application you need to change the config file with the access data for your database. The file is

> config/db-config.json

And now you are ready to run the backend with

> node index.js

The application will listen on the following url :

> http://localhost:3000

Two endpoints are exposed:

> http://localhost:3000/tree
> http://localhpst:3000/tree/:id

The first will give you the dafault tree structure and the second will give you the possibility to choose the root

# nerf-herders-frontend

This module is responsible of the frontend and it is built with VueJS.

To install the dependencies please run

> npm install

and to run the application you can run

> npm run serve

the application should run on

> http://localhost:8080

where it is also serving the single webpage
