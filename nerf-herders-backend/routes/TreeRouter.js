const Node = require('../model/Node')
const Edge = require('../model/Edge')
const TreeNode = require('../model/TreeNode')
const express = require('express')
const driver = require('../config/db-config')

const router = express.Router()



router.get('/:id', async (req, res) => {
    const result = await getTree(req.params.id)

    if(result == null) {
        res.status(404).send();
    } else {
        res.send(JSON.stringify(result))
    }

})

router.get('/', async (req, res) => {
    const result = await getTree('A')

    if(result == null) {
        res.status(404).send();
    } else {
        res.send(JSON.stringify(result))
    }
})

const getTree = async function (rootName) {
    const session = driver.session()
    try {
        const result = await session.run(
            'MATCH (p:Node)-[r*0..]->(:Node) ' +
            'RETURN collect(DISTINCT p) as nodes, [r in collect(distinct last(r)) | [startNode(r).name,endNode(r).name]] as edges'
        ).then(result => getTreeStructure(rootName, result))

        return result;

    } finally {
        await session.close()
    }
}

const getTreeStructure = function (rootName, data) {

    var nodes = new Map();
    data.records[0]._fields[0].forEach( n => {
        nodes[n.properties.name] = new TreeNode(new Node(n.properties.name, n.properties.description), null)
    })

    var edges = data.records[0]._fields[1].map( n => {
        return new Edge(n[0], n[1])
    })

    if(nodes[rootName] == null) {
        return null;
    }

    var hasParent = new Map()
    var root = nodes[rootName];

    edges.forEach(edge => {

        nodes[edge.startNodeName].children.push(nodes[edge.endNodeName])

        hasParent[edge.endNodeName] = true
    })

    return root;
}

const validateParamId = function(id){
    return !id.isEmpty
}


module.exports = router;
