const express = require('express')
const app = express()
const port = 3000

app.use(function (req,res,next) {
    // You could use * instead of the url below to allow any origin,
    // but be careful, you're opening yourself up to all sorts of things!
    res.setHeader('Access-Control-Allow-Origin',  "http://localhost:8080");
    next()
})
    .use(express.urlencoded({ extended: false }))

app.use('/tree', require('./routes/TreeRouter'))

app.listen(port, () => {
    console.log(`Nerf-Herder app listening at http://localhost:${port}`)
})


