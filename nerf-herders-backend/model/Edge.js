class Edge {
    constructor(startNodeName, endNodeName) {
        this.startNodeName = startNodeName
        this.endNodeName = endNodeName
    }
}

module.exports = Edge;
