class TreeNode {
    constructor(node, children) {
        this.node = node
        this.children = children == null ? [] : [child]
    }
}

module.exports = TreeNode;
