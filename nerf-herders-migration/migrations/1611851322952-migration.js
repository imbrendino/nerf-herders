'use strict'
const fs = require('fs');
var driver = require('../db-config')

module.exports.up = async function (next) {
  const session = driver.session()

  let rawdata = fs.readFileSync('data.json');
  let jsonData = JSON.parse(rawdata);

  try {
    for (const node of jsonData.data) {
      await session.run(
          'CREATE (a:Node {name: $name, description: $description} ) RETURN a',
          {name: node.name, description: node.description}
      )
    }

    for (const node of jsonData.data) {
      if (node.parent != null && node.parent.length > 0) {
        await session.run(
            'MATCH (a:Node),(b:Node) WHERE a.name = $name AND b.name = $parent CREATE (a)<-[r:Child]-(b) RETURN type(r)',
            {name: node.name, parent: node.parent}
        )
      }
    }
  } finally {
    await session.close()
    await driver.close()
  }
}

module.exports.down = async function (next) {
  const session = driver.session()

  let rawdata = fs.readFileSync('data.json');
  let jsonData = JSON.parse(rawdata);

  try {
    for (const node of jsonData.data) {
      await session.run(
          'MATCH (n:Node { name: $name }) DETACH DELETE n',
          {name: node.name}
      )
    }
  } finally {
    await session.close()
    await driver.close()
  }
}
